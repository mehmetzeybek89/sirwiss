<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    public function run()
    {
        // Kullanıcı oluştur
        $user = User::create([
            'name' => 'Default User',
            'email' => 'defaultuser@example.com',
            'password' => bcrypt('password'),
        ]);

        // Ürün oluştur
        $product = Product::create([
            'name' => 'Default Product',
            'price' => 50.00,
            'status' => 'active',
            'user_id' => $user->id,
            'type' => 'goods',
        ]);
    }
}
