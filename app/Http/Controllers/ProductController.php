<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductHistory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProductAdded;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $query = Product::query();


        if ($request->has('product')) {
            $query->where('name', 'like', '%' . $request->input('product') . '%');
        }

        if ($request->has('user')) {
            $query->whereHas('user', function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->input('user') . '%');
            });
        }

        $products = $query->get();


        return response()->json(['products' => $products]);
    }

    public function show($id)
    {
        try {
            $product = Product::findOrFail($id);
            return response()->json(['product' => $product]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => 'Product Not Found'], 404);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
            'status' => 'required|in:active,inactive',
            'type' => 'required|in:goods,service',
        ]);

        $product = Product::create([
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'status' => $request->input('status'),
            'user_id' => auth()->user()->id,
            'type' => $request->input('type'),
        ]);

        //Mail::to(auth()->user()->email)->send(new ProductAdded($product));

        return response()->json(['product' => $product], 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'string',
            'price' => 'numeric',
            'status' => 'in:active,inactive',
            'type' => 'in:goods,service',
        ]);

        $product = Product::findOrFail($id);

        $oldData = $product->toArray();

        $product->update([
            'name' => $request->input('name', $product->name),
            'price' => $request->input('price', $product->price),
            'status' => $request->input('status', $product->status),
            'type' => $request->input('type', $product->type),
        ]);

        $newData = $product->toArray(); // Yeni veriyi al

        $this->updateHistory($product, $oldData, $newData);

        return response()->json(['product' => $product]);
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return response()->json(['message' => 'Product deleted successfully']);
    }

    private function updateHistory(Product $product, array $oldData, array $newData)
    {
        ProductHistory::create([
            'product_id' => $product->id,
            'user_id' => Auth::user()->id,
            'old_data' => $oldData,
            'new_data' => $newData,
        ]);
    }


}
