<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class RandomUserMiddleware
{
    public function handle($request, Closure $next)
    {

        $randomUser = User::inRandomOrder()->first();

        auth()->login($randomUser);

        return $next($request);
    }
}
