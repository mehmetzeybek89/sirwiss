# Sirwiss Products API

Sirwiss Products API, ürün ekleme, silme, güncelleme ve filtre bazlı listeleme özelliklerini sağlayan bir API'dir.

## Kullanım

API'yi kullanmak için aşağıdaki adımları izleyin:

1. Proje URL: [https://gitlab.com/mehmetzeybek89/sirwiss](https://gitlab.com/mehmetzeybek89/sirwiss)


2. **Projeyi Klonlayın:**
   ```bash
   git clone https://gitlab.com/mehmetzeybek89/sirwiss.git  &&  cd sirwiss
3. **.env Dosyasını Oluşturun:**
    ```bash
    cp .env.example .env
4. **Composer Bağımlılıklarını Yükleyin:**
    ```bash
    composer install
5. **Key Oluşturun**
    ```bash
    php artisan key:generate
5. **Veritabanını Migrate Edin:**
    ```bash
    php artisan migrate
6. **Seeder ile Veritabanını Doldurun:**
    ```bash
    php artisan db:seed
7. **Laravel Development Sunucusunu Başlatın:**
    ```bash
    php artisan serve


Bu adımdan sonra, http://localhost:8000 adresinde API'ye erişebilirsiniz. Özellikleri ve kullanımı anlamak için Swagger arayüzüne de göz atabilirsiniz.
# Swagger Url:
[http://localhost:8000/documentation](http://localhost:8000/documentation)

.env dosyasında düzenlemeyi unutmayın
```bash
APP_URL=http://localhost:8000